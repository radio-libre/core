/**
 * IMPORTANT!!
 * 
 * THE FUNCTIONS IN THIS FILE ARE NOT MEANT TO BE
 * RAN IN THE BROWSER. THEY ARE SUPPOSED TO BE 
 * EXECUTED IN A NODEJS PROCESS BEFORE STARTING KARMAJS
 */
import * as http from 'http';
import { Readable } from 'stream';
import { AddressInfo } from 'net';

export function httpServeStream(rs: ()=>Readable, options: {
    port: number,
    mimetype: string
}): Promise<string | AddressInfo>{
    return new Promise((resolve, reject) => {
        const server = http.createServer((req, res) => {
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.setHeader('Content-Type', options.mimetype);
            res.statusCode = 200;
            rs().pipe(res);
        });
        server.on('error', reject);
        server.listen(options.port, () => {
            const address = server.address();
            return resolve(address);
        });
    });
}