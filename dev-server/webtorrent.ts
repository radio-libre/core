/**
 * IMPORTANT!!
 * 
 * THE FUNCTIONS IN THIS FILE ARE NOT MEANT TO BE
 * RAN IN THE BROWSER. THEY ARE SUPPOSED TO BE 
 * EXECUTED IN A NODEJS PROCESS BEFORE STARTING KARMAJS
 */
import * as WebTorrent from 'webtorrent';
import { Readable } from 'stream';

export function webtorrentServeStream(rs: ()=>Readable, options: {
    client: WebTorrent.Instance,
    name: string
}): Promise<string> {
    return new Promise((resolve, reject) => {
        options.client.on('error', reject);
        options.client.seed(rs(), {name: options.name}, function (torrent: WebTorrent.Torrent) {
            options.client.removeListener('error', reject);
            return resolve(torrent.magnetURI);
        });
    });
}