import * as fs from 'fs';
import {join} from 'path';
import { AddressInfo } from 'net';
import { chain } from 'stream-chain';
import * as WebTorrent from "webtorrent";
import { Readable } from 'stream';

import { httpServeStream } from "./http";
import { TrackStream } from '../src/helpers/streams/tracks';
import { Track2JsonStream } from '../src/helpers/streams/tracks2json';
import { webtorrentServeStream } from './webtorrent';

export function jsonFileStream(size: number) {
    return new Readable().wrap(chain([
        new TrackStream(size),
        new Track2JsonStream(size)
    ]));
}

export interface DevServerStateInfo {
    timestamp: number;
    pid: number;
    json: {
        http: string | AddressInfo,
        webtorrent: string
    },
    audio: {
        http: string | AddressInfo,
        webtorrent: string
    }
}

export async function createDevServer(client: WebTorrent.Instance, port: number): Promise<DevServerStateInfo>{
    console.log("[DevServer] Initializing server.");
    const jsonStream = ()=>jsonFileStream(12);
    const audioStream = ()=>fs.createReadStream(join(__dirname, "sample.mp3"));

    const [
        jsonHttpServerInfo,
        jsonMagnetURI,
        audioHttpServerInfo,
        audioMagnetURI
    ] = await Promise.all([
        httpServeStream(jsonStream, {port, mimetype: "application/json"}),
        webtorrentServeStream(jsonStream, {client, name: 'sample.mp3'}),
        httpServeStream(audioStream, {port: port+1, mimetype: "audio/mp3"}),
        webtorrentServeStream(audioStream, {client, name: 'sample.mp3'})
    ]);

    const stateInfo = {
        timestamp: Date.now(),
        pid: process.pid,
        json: {
            http: jsonHttpServerInfo,
            webtorrent: jsonMagnetURI
        },
        audio: {
            http: audioHttpServerInfo,
            webtorrent: audioMagnetURI
        }
    }
    console.log("[DevServer] Info:  ", stateInfo);
    console.log("[DevServer] Ready for connections.");
    return stateInfo;
}