const path = require("path");

module.exports = function (env) {
  const productionBuild = env === "production";
  const filename = `rlcore${productionBuild ? ".min" : ""}.js`;

  return {
    target: "web",
    node: {
      fs: "empty",
    },
    mode: productionBuild ? "production" : "development",
    entry: "./src/index.ts",
    devtool: "source-map",
    resolve: {
      extensions: [".ts", ".js"],
    },
    optimization: {
      concatenateModules: productionBuild,
      minimize: productionBuild,
    },
    output: {
      filename: filename,
      path: path.resolve(__dirname, "lib"),
      libraryTarget: "umd",
      globalObject: "this",
    },
    module: {
      rules: [
        {
          test: /\.ts$/,
          use: ["ts-loader"],
          exclude: /(node_modules)/,
        },
        {
          test: /\.ts$/,
          enforce: "pre",
          use: [
            {
              loader: "eslint-loader",
              options: {
                fix: true,
              },
            },
          ],
          exclude: /(node_modules)/,
        },
      ],
    },
  };
};
