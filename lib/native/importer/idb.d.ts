/// <reference types="node" />
import { IDBPDatabase } from 'idb';
import { Model } from '../../models';
import { Writable } from 'stream';
import { NativeStoreFactory, NativeStoreImporter } from '../meta';
export declare const idbImporterFactoryFactory: (dbName: string, bucketSize: number) => NativeStoreFactory<NativeStoreImporter>;
export declare class IDBStoreImporter<T extends Model> extends Writable {
    private db;
    private storeName;
    private bucketSize;
    private bucket;
    constructor(db: IDBPDatabase, storeName: string, bucketSize: number);
    flushBucket(): Promise<void>;
    _write(chunk: T, enc: string, cb: (err?: Error) => void): Promise<void>;
    _final(): Promise<void>;
    _destroy(): void;
}
