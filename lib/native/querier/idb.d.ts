import { NativeStoreFactory, NativeStoreQuerier } from '../meta';
import { Model } from '../../models';
export declare function idbQuerierFactoryFactory<T extends Model>(dbName?: string): NativeStoreFactory<NativeStoreQuerier<T>>;
