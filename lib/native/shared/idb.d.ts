import { IDBPDatabase } from "idb";
export interface OpenIDBOptions {
    name: string;
    version: number;
    store: string;
}
export declare function openIDBDatabase(options: OpenIDBOptions): Promise<IDBPDatabase>;
