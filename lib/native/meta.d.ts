/// <reference types="node" />
import { Writable } from "stream";
export declare type NativeStoreImporter = Writable;
export interface NativeStoreQuerier<T> {
    count(): Promise<number>;
    cursor(query?: string): AsyncIterable<Promise<T>>;
}
export declare type NativeStoreImportersMap = {
    [id: string]: NativeStoreFactory<NativeStoreImporter>;
};
export declare type NativeStoreQueriersMap<T> = {
    [id: string]: NativeStoreFactory<NativeStoreQuerier<T>>;
};
export declare type NativeStoreFactory<Target> = (meta: NativeStoreMeta) => Promise<Target>;
export interface NativeStoreMeta {
    imp: string;
    qry: string;
    src: string;
    nme: string;
    ver: number;
    lnk: string;
    eml: string;
}
export interface NativeStore<T> {
    importerFactory: NativeStoreFactory<NativeStoreImporter>;
    querierFactory: NativeStoreFactory<NativeStoreQuerier<T>>;
    meta: NativeStoreMeta;
}
export declare class NativeStoreMetaParser extends URLSearchParams {
    readonly query: string;
    static NATIVE_STORE_IMPORTERS(): NativeStoreImportersMap;
    static NATIVE_STORE_QUERIERS<T>(): NativeStoreQueriersMap<T>;
    static encoder(meta: NativeStoreMeta): string;
    constructor(query: string);
    get(key: string): string;
    private importerFactory;
    private querierFactory;
    private version;
    parse<T>(): NativeStore<T>;
}
