/// <reference types="node" />
import { Readable, Writable, Duplex, Transform } from 'stream';
export interface Model {
    id?: string;
}
export interface Track extends Model {
    title: string;
    artist: string;
    album: string;
    year: number;
    comment: string;
    genre: number;
    trackNumber: number;
    srcs: string[];
}
export declare type TransformFunction = (chunk: any, encoding?: string) => any;
export declare type Stream = Readable | Writable | Duplex | Transform;
export declare type StreamItem = Stream | TransformFunction;
