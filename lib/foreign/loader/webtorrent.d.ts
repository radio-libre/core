import * as WebTorrent from 'webtorrent';
import { ForeignStoreFactory, ForeignStoreLoader } from '../meta';
export declare function webtorrentLoaderFactoryFactory(client: WebTorrent.Instance, findFunc?: (file: WebTorrent.TorrentFile) => boolean): ForeignStoreFactory<ForeignStoreLoader>;
