/// <reference types="node" />
import { Readable } from "stream";
import { StreamItem } from "src/models";
import WebTorrent = require("webtorrent");
export declare type ForeignStoreFactory<Target> = (meta: ForeignStoreMeta) => Promise<Target>;
export declare type ForeignStoreLoader = Readable;
export declare type ForeignStoreDecoder = StreamItem[];
export declare type ForeignStoreParser = StreamItem[];
export declare type ForeignStoreLoadersMap = {
    [id: string]: ForeignStoreFactory<ForeignStoreLoader>;
};
export declare type ForeignStoreDecodersMap = {
    [id: string]: ForeignStoreFactory<ForeignStoreDecoder>;
};
export declare type ForeignStoreParsersMap = {
    [id: string]: ForeignStoreFactory<ForeignStoreParser>;
};
export interface ForeignStoreMeta {
    ldr: string;
    dec: string;
    psr: string;
    src: string;
    nme: string;
    ver: number;
    lnk: string;
    eml: string;
}
export interface ForeignStore {
    loaderFactory: ForeignStoreFactory<ForeignStoreLoader>;
    decoderFactory: ForeignStoreFactory<ForeignStoreDecoder>;
    parserFactory: ForeignStoreFactory<ForeignStoreParser>;
    meta: ForeignStoreMeta;
}
export declare class ForeignStoreMetaParser extends URLSearchParams {
    readonly query: string;
    private client;
    private FOREIGN_STORE_LOADERS;
    private FOREIGN_STORE_DECODERS;
    private FOREIGN_STORE_PARSERS;
    static encoder(meta: ForeignStoreMeta): string;
    constructor(query: string, client?: WebTorrent.Instance);
    get(key: string): string;
    private loaderFactory;
    private decoderFactory;
    private parserFactory;
    private version;
    parse(): ForeignStore;
}
