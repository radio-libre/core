import WebTorrent = require('webtorrent');
import { MusicStorePlayer } from '../meta';
export declare function webtorrentPlayerFactory(client: WebTorrent.Instance, parent?: string, elementOptions?: {
    autoplay?: boolean;
    controls?: boolean;
    maxBlobLength?: number;
}): MusicStorePlayer;
