import { MusicStorePlayer } from '../meta';
export declare function youtubePlayerFactory(proxyHost?: string, parent?: string, elementOptions?: {
    autoplay?: boolean;
    controls?: boolean;
    maxBlobLength?: number;
}): MusicStorePlayer;
