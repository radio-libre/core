import { MusicStorePlayer } from "../meta";
export declare function httpPlayerFactory(doc: Document, parent?: string, elementOptions?: {
    autoplay?: boolean;
    controls?: boolean;
}): MusicStorePlayer;
