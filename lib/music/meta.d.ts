import WebTorrent = require("webtorrent");
export declare type MusicStorePlayer = (meta: MusicStoreMeta) => Promise<HTMLMediaElement>;
export declare type MusicStorePlayersMap = {
    [id: string]: MusicStorePlayer;
};
export interface MusicStoreMeta {
    plr: string;
    src: string;
    fmt: string;
}
export interface MusicStore {
    player: MusicStorePlayer;
    meta: MusicStoreMeta;
}
export declare class MusicStoreMetaParser extends URLSearchParams {
    readonly query: string;
    private client;
    private MUSIC_STORE_PLAYERS;
    static encode(meta: MusicStoreMeta): string;
    constructor(query: string, client?: WebTorrent.Instance);
    get(key: string): string;
    private player;
    parse(): MusicStore;
}
