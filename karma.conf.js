const webpack = require("webpack");

let webpackConfig = require("./webpack.config")();

module.exports = function (config) {
  config.set({
    basePath: "",
    frameworks: ["jasmine"],
    plugins: [
      require("karma-jasmine"),
      require("karma-firefox-launcher"),
      require("karma-coverage-istanbul-reporter"),
      require("karma-webpack"),
      require("karma-jasmine-html-reporter"),
    ],
    files: [
      {
        pattern: "./src/**/*.spec.ts",
        watched: false,
      },
    ],
    preprocessors: {
      "./src/**/*.ts": ["webpack"],
    },
    mime: {
      "text/x-typescript": ["ts"],
    },
    browserNoActivityTimeout: 120000,
    webpack: {
      module: webpackConfig.module,
      resolve: webpackConfig.resolve,
      mode: webpackConfig.mode,
      node: webpackConfig.node,
      target: webpackConfig.target,
      plugins: [new webpack.ProvidePlugin({ 'window.indexedDB': 'fake-indexeddb' })],
    },
    coverageIstanbulReporter: {
      reports: ["html", "lcovonly", "text-summary"],
      fixWebpackSourcePaths: true,
    },
    remapIstanbulReporter: {
      reports: {
        html: "coverage",
        lcovonly: "./coverage/coverage.lcov",
        "text-summary": "",
      },
    },
    customHeaders: [
      {
        match: ".*",
        name: "Access-Control-Allow-Origin",
        value: "*",
      },
    ],
    reporters: ["progress", "kjhtml"],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ["FirefoxHeadless"],
    singleRun: false,
  });
};
