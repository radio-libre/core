# Radio Libre Core
Core functionality of the Radio Libre Project.

## Concept
There are three store types:
- `Foreign Store`: An abstraction for a database that will never be queried, but whose data will be imported into a `Native Store`
- `Native Store`: A queriable database that imports its data from a `Foreign Store`.
- `Music Store`: A location where playable music data can be fetched

Store data is serialized into a string parsed using a `MetaParser` subclass. This string is sharable through users so they can publish store locations.

The lib is meant to scale horizontally by adding loaders/decoders/parsers/players/importers/queriers.

## Components
Description of the six core components:

**Name** | **Type** | **Purpose** | **Input** | **Output** | **Example Engines**
--- | --- | --- | --- | --- | ---
Loader | `Readable` | Load raw data from a location (anywhere) | Foreign Store Location | bytes | webtorrent, http
Decoder | ` StreamItem[]` | Decode raw data fetched by loader | bytes | string or bytes| gzip, none
Parser | `StreamItem[]` | Transform byte stream into object stream | bytes | Objects | json
Player | `(meta: MusicStoreMeta) => Promise<HTMLMediaElement>` | Fetch music source anywhere into a playable HTMLMediaElement | MusicStoreMeta | HTMLMediaElement | youtube, webtorrent, http
Importer | `Writable` | Stream-write objects into a queriable database | Objects | void | idb
Querier | `NativeStoreQuerier<T>` | Query a database | ---- | ---- | idb

**Note**: 
```ts
type StreamItem = Readable | Writable | Transform | Duplex | (chunk: any, encoding?: string) => any
```

## Testing
The lib uses KarmaJS and Jasmine for its tests. Furthermore, we added a development server (`dev-server/`) to make integration tests with webtorrent and http. Most of the components being a really straight-forward wrapper around another library, unit tests would be pointless for them. In order to make sure these components work, we have to test them in a real environment.

To run the tests:
```sh
./test.ts
# OR
npx ts-node test.ts
```

## Examples
### Foreign Store
```ts
import {ForeignStoreMetaParser} from 'rlcore';

// Encode data
const encoded = ForeignStoreMetaParser.encode({
    ldr: 'webtorrent'; // Loader
    dec: 'gzip'; // Decoder
    psr: 'json'; // Parser
    src: '<magnetURI>'; // Source
    nme: 'unique-identifier'; // Name
    ver: 1'; // Version
    lnk: 'https://example.org/'; // Info Link
    eml: 'report-error@databaseadmin.org'; // Report Email
});

const parser = new ForeignStoreMetaParser(encoded);
const store = parser.parse();
// a read stream that streams track objects
const readstream = await ForeignStoreMetaParser.createReadStream(store);
```

### Native Store
```ts
import {NativeStoreMetaParser} from 'rlcore';

// Encode data
const encoded = NativeStoreMetaParser.encode({
    imp: 'idb'; // Importer
    qry: 'idb'; // Querier
    src: 'none'; // Source (not used for IDB)
    nme: 'unique-store-identifier'; // Name
    ver: 1'; // Version
    lnk: 'https://example.org/'; // Info Link
    eml: 'error-report@databaseadmin.org'; // Report Email
});

const parser = new NativeStoreMetaParser(encoded);
const store = parser.parse();
// a write stream to streams track objects to db (to import tracks)
const writeStream = await NativeStoreMetaParser.createWriteStream(store);

const querier = await NativeStoreMetaParser.createQuerier(store);

// Total tracks count
const tracksCount = await querier.count();

// Tracks Iterator
for await (const track of querier.cursor()){
    console.log(track);
}

// Don't forget to close!
querier.close();
```

### Music Store
```ts
import {MusicStoreMetaParser} from 'rlcore';

// Encode data
const encoded = MusicStoreMetaParser.encode({
    plr: "webtorrent"; // Player backend
    src: "<magnet_uri>"; // Source
    fmt: "mp4"; // Format
});

const parser = new MusicStoreMetaParser(encoded);
const store = parser.parse();

const htmlMediaElement = await store.player(store.meta);
await htmlMediaElement.play();
htmlMediaElement.pause();
```