#!./node_modules/.bin/ts-node-script
import { createDevServer } from './dev-server'
import { join } from 'path';
import * as WebTorrent from 'webtorrent-hybrid';
import * as fs from 'fs';
import karma = require('karma'); // we use require() because of error TS2451

async function saveStateToFile(state: any) {
    const filename = join(__dirname, "test-server-info.spec.json");
    return new Promise<void>((resolve, reject) => {
        fs.writeFile(filename, JSON.stringify(state, null, "    "), (err) => {
            if (err) return reject(err);
            return resolve();
        });
    });
}

(async () => {
    const client = new WebTorrent();
    const devServerInfo = await createDevServer(client, 1234);
    await saveStateToFile(devServerInfo);
    const karmaServer = new karma.Server({
        configFile: join(__dirname, 'karma.conf.js')
    });
    karmaServer.start();
})();
