import { httpPlayerFactory } from "./player/http";
import { youtubePlayerFactory } from "./player/youtube";
import { webtorrentPlayerFactory } from "./player/webtorrent";
import WebTorrent = require("webtorrent");
import { MetaParser } from "../meta";

export type MusicStorePlayer = (meta: MusicStoreMeta) => Promise<HTMLMediaElement>;

export type MusicStorePlayersMap = { [id: string]: MusicStorePlayer };

export type HTMLMediaElementOptions = {
    autoplay?: boolean,
    controls?: boolean,
}

export interface MusicStoreMeta {
    plr: string; // Player
    src: string; // Source
    fmt: string; // Format
}

export interface MusicStore {
    player: MusicStorePlayer;
    meta: MusicStoreMeta;
}


export class MusicStoreMetaParser extends MetaParser<MusicStoreMeta> {
    static PLAYERS_PRIORITY: string[] = [ "yt", "webtorrent", "http" ];

    static parseAndSanitizeMetas(
        srcs: string[],
        client?: WebTorrent.Instance,
        parent?: string,
        elementOptions?: {
            autoplay?: boolean,
            controls?: boolean
            maxBlobLength?: number,
        } 
    ): MusicStore[] {
        return srcs
            .map((src: string)=>{
                try {
                    return new MusicStoreMetaParser(src, client, parent, elementOptions).parse();
                } catch (err) {
                    return null;
                }
            })
            .filter((store)=>!!store)
            .sort((store1, store2)=>{
                const idx1 = this.PLAYERS_PRIORITY.indexOf(store1.meta.plr);
                const idx2 = this.PLAYERS_PRIORITY.indexOf(store2.meta.plr); 
                if (idx1 === idx2) {
                    return 0;
                } else if (idx1 > idx2) {
                    return 1;
                } else if (idx1 < idx2) {
                    return -1;
                }
            })
    }

    /******************************
     * REGISTER THE PLAYERS HERE
     ******************************/
    private MUSIC_STORE_PLAYERS(): MusicStorePlayersMap {
        return {
            "http": httpPlayerFactory(window.document, this.parent, this.elementOptions),
            "yt": youtubePlayerFactory({
                host: 'cors-anywhere.herokuapp.com',
                port: 443,
                protocol: 'https:'
            }, this.parent, this.elementOptions),
            "webtorrent": webtorrentPlayerFactory(this.client, this.parent, this.elementOptions)
        };
    }

    constructor(
        public readonly query: string,
        private client: WebTorrent.Instance = null,
        private parent: string = 'body',
        private elementOptions: {
            autoplay?: boolean,
            controls?: boolean
            maxBlobLength?: number,
        } = {
                autoplay: true,
                controls: false
            }
    ) {
        super(query, ["plr", "src", "fmt"]);

        // Throw errors in constructor
        this.player();
    }

    private player(): MusicStorePlayer {
        if (this.get("plr") === 'webtorrent' && !this.client)
            throw new Error(`Player is WebTorrent but no client was passed to constructor.`);

        const plr = this.MUSIC_STORE_PLAYERS()[this.get("plr")];
        if (!plr) throw new Error(`${this.get("plr")} is not a valid player backend.`);
        return plr;
    }

    parse(): MusicStore {
        return {
            player: this.player(),
            meta: this.parseMeta()
        }
    }
}

