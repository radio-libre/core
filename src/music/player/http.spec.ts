import {httpPlayerFactory} from './http';
import {htmlMediaIsPlayable} from '../../helpers/utils/htmlmedia';
import * as DevServerInfo from '../../../test-server-info.spec.json';

describe('Player - HTTP', ()=>{
    it('returns valid media element if source is reachable', async (done)=>{
        const player = httpPlayerFactory(window.document, 'body', {});
        const elt = await player({
            plr: 'http',
            src: `http://localhost:${DevServerInfo.audio.http.port}/`,
            fmt: 'mp3'
        });
        expect(await htmlMediaIsPlayable(elt)).toEqual(true);
        done();
    }, 20000);

    it('fails if source is unreachable', async (done)=>{
        const player = httpPlayerFactory(window.document, 'body', {});
        const elt = await player({
            plr: 'http',
            src: `http://127.0.0.2/unreachable`,
            fmt: 'mp3'
        });
        expect(await htmlMediaIsPlayable(elt)).toEqual(false);
        done();
    }, 20000);
});