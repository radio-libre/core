import { webtorrentPlayerFactory } from './webtorrent';
import { htmlMediaIsPlayable } from '../../helpers/utils/htmlmedia';
import * as DevServerInfo from '../../../test-server-info.spec.json';
import { MusicStorePlayer } from '../meta';
import * as WebTorrent from 'webtorrent';

describe('Player - WebTorrent', () => {
    let player: MusicStorePlayer;
    let client: WebTorrent.Instance;
    beforeEach(async () => {
        client = new WebTorrent();
        player = webtorrentPlayerFactory(client, 'body', {});
    });

    afterEach((done) => {
        client.destroy((err) => {
            if (err) throw err;
            done();
        });
        player = null;
    });

    it('returns valid media element if source is reachable', async (done) => {
        const elt = await player({
            plr: 'http',
            src: DevServerInfo.audio.webtorrent,
            fmt: 'mp3'
        });
        expect(await htmlMediaIsPlayable(elt)).toEqual(true);
        done();
    }, 120000);

    it('fails if source is unreachable', (done) => {
        player({
            plr: 'http',
            src: 'not-a-magnet',
            fmt: 'mp3'
        }).then((media: HTMLMediaElement) => {
            throw new Error("Source wasn't supposed to be reachable. It returned " + media);
        }).catch((err) => {
            expect(err.message).toMatch(/Invalid torrent/);
            done();
        });
    }, 120000);

    it('fails if torrent contains no playable file', (done) => {
        player({
            plr: 'http',
            src: DevServerInfo.audio.webtorrent,
            fmt: 'wav'
        }).then((media: HTMLMediaElement)=>{
            throw new Error("Source wasn't supposed to be reachable. It returned " + media);
        }).catch((err)=>{
            expect(err.message).toMatch(/There is no playable file in the given torrent/);
            done();
        });
    }, 120000);
});