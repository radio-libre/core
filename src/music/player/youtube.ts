import * as ytdl from 'ytdl-core';
import render = require('render-media');
import { MusicStorePlayer, MusicStoreMeta, HTMLMediaElementOptions } from '../meta';
import { RequestOptions } from 'https';

export type CORSProxy = {
    host: string;
    port: number;
    protocol: string;
}

export function youtubePlayerFactory(
    proxy: CORSProxy,
    parent: string,
    elementOptions: HTMLMediaElementOptions
): MusicStorePlayer {
    return async (meta: MusicStoreMeta) => {
        const info = await getVideoInfo(meta.src, proxy, "highestaudio");
        const format = getBestFormat(info.formats);
        const element = await createYtElement(info, format, proxy, parent, elementOptions);
        element.controls = false;
        return element;
    }
}

function getBestFormat(formats: ytdl.videoFormat[]) {
    const audioOnlyFormats = formats.filter(fmt => !fmt.hasVideo && fmt.hasAudio);
    if (audioOnlyFormats.length === 0)
        throw new Error("There is no available audio format for the given video.");
    return audioOnlyFormats[0];
}

function defaultYtdlOptions(proxy: CORSProxy): ytdl.downloadOptions {
    return {
        requestOptions: {
            transform: (parsed: RequestOptions) => {
                return {
                    host: proxy.host,
                    port: proxy.port,
                    protocol: proxy.protocol,
                    path: `/${parsed.protocol}//${parsed.host}${parsed.path}`,
                    maxRedirects: 10,
                    headers: parsed.headers
                }
            },
        }
    }
}

function createYtElement(info: ytdl.videoInfo, format: ytdl.videoFormat,
    proxy: CORSProxy, parent: string, elementOptions: {
        autoplay?: boolean,
        controls?: boolean,
        maxBlobLength?: number,
    }) {
    return new Promise<HTMLMediaElement>((resolve, reject) => {
        const file = {
            name: `yt-${info.videoDetails.videoId}.${format.container}`,
            createReadStream: (opts?: { start?: number, end?: number }) => {
                return createReadStreamFromInfo(info, format, proxy, opts || {});
            }
        }
        render.append(file, parent, elementOptions, function (err: Error, elem: HTMLMediaElement) {
            if (err) return reject(err);

            return resolve(elem);
        })
    });
}

function createReadStreamFromInfo(info: ytdl.videoInfo, format: ytdl.videoFormat,
    proxy: CORSProxy, range: { start?: number, end?: number }) {
    const rge = { start: range.start || 0, end: range.end }
    const rs = ytdl.downloadFromInfo(info, {
        ...defaultYtdlOptions(proxy),
        format,
        range: rge
    });
    return rs;
}

async function getVideoInfo(id: string, proxy: CORSProxy, quality: "highestaudio" | "lowestaudio" = "highestaudio") {
    return (await ytdl.getInfo("https://www.youtube.com/watch?v=" + id, {
        ...defaultYtdlOptions(proxy),
        quality
    }));
}