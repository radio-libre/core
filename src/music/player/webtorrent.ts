import WebTorrent = require('webtorrent');
import { MusicStorePlayer, MusicStoreMeta, HTMLMediaElementOptions } from '../meta';

export function webtorrentPlayerFactory(
    client: WebTorrent.Instance,
    parent: string,
    elementOptions: HTMLMediaElementOptions
): MusicStorePlayer {
    return async (meta: MusicStoreMeta) => {
        return new Promise<HTMLMediaElement>((resolve, reject) => {
            const errorHandler = (err: Error) => {
                if (!err.message.includes("RTCPeerConnection is gone (did you enter Offline mode?)")) {
                    return reject(err);
                } else {
                    console.warn(err);
                }
            }
            client.on('error', errorHandler);
            client.add(meta.src, (torrent) => {
                const f = torrent.files.find(file => file.name.endsWith(`.${meta.fmt}`));
                if (!f) {
                    client.removeListener('error', errorHandler);
                    return reject(new Error("There is no playable file in the given torrent."));
                }
                f.appendTo(parent, elementOptions, (err, element) => {
                    if (err) return reject(err);
                    client.removeListener('error', errorHandler);
                    return resolve(element);
                });
            });
        });
    }
}
