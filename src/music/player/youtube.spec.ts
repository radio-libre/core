import {youtubePlayerFactory} from './youtube';
import {htmlMediaIsPlayable} from '../../helpers/utils/htmlmedia';

const NOT_A_VIDEO_ID = "3kHbZRMAOpW";
const VALID_ID = "z1DL-G_XlG0";
const INVALID_ID = "not-an-id";

describe('Player - Youtube', ()=>{
    it('works if proxy is "cors-anywhere.herokuapp.com"', async (done)=>{
        const player = youtubePlayerFactory({
            host: 'cors-anywhere.herokuapp.com',
            port: 443,
            protocol: 'https:'
        }, 'body', {autoplay: false, controls: false});
        const element = await player({
            plr: 'yt',
            src: VALID_ID,
            fmt: 'unused'
        });
        expect(await htmlMediaIsPlayable(element)).toEqual(true);
        done();
    }, 120000);

    it("fails if id isn't used by a video but is valid", (done)=>{
        const player = youtubePlayerFactory({
            host: 'cors-anywhere.herokuapp.com',
            port: 443,
            protocol: 'https:'
        }, 'body', {autoplay: false, controls: false});
        player({
            plr: 'yt',
            src: NOT_A_VIDEO_ID,
            fmt: 'unused'
        }).then((media: HTMLMediaElement)=>{
            throw new Error("Unexpected media element from not-a-video youtube id: " + media.toString());
        }).catch((err) => {
            expect(err.message).toMatch(/Video unavailable/);
            done();
        });
    });
    
    it('fails if invalid id', (done)=>{
        const player = youtubePlayerFactory({
            host: 'cors-anywhere.herokuapp.com',
            port: 443,
            protocol: 'https:'
        }, 'body', {autoplay: false, controls: false});
        player({
            plr: 'yt',
            src: INVALID_ID,
            fmt: 'unused'
        }).then((media: HTMLMediaElement)=>{
            throw new Error("Unexpected media element from invalid youtube id: " + media.toString());
        }).catch((err) => {
            expect(err.message).toContain(`Video id (${INVALID_ID}) does not match expected format`);
            done();
        });
    });
});