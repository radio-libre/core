import { MusicStorePlayer, MusicStoreMeta, HTMLMediaElementOptions } from "../meta";

export function httpPlayerFactory(
    doc: Document,
    parent:string,
    elementOptions: HTMLMediaElementOptions) : MusicStorePlayer {
    return async (meta: MusicStoreMeta) => {
        const audio = doc.createElement("audio");
        audio.src = meta.src;
        audio.controls = typeof elementOptions.controls === 'boolean' ? elementOptions.controls : false;
        audio.autoplay = typeof elementOptions.controls === 'boolean' ? elementOptions.autoplay : false;
        doc.querySelector(parent).append(audio);
        return audio;
    }
}