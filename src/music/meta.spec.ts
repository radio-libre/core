import {MusicStoreMetaParser} from './meta';
import * as WebTorrentPlayer from './player/webtorrent';
import WebTorrent = require('webtorrent');

const WEBTORRENT_VALID = "plr=d2VidG9ycmVudA==&src=bWFnbmV0Oj94dD11cm46YnRpaDowOGFkYTVhN2E2MTgzYWFlMWUwOWQ4MzFkZjY3NDhkNTY2MDk1YTEwJmRuPVNpbnRlbCZ0cj11ZHAlM0ElMkYlMkZleHBsb2RpZS5vcmclM0E2OTY5JnRyPXVkcCUzQSUyRiUyRnRyYWNrZXIuY29wcGVyc3VyZmVyLnRrJTNBNjk2OSZ0cj11ZHAlM0ElMkYlMkZ0cmFja2VyLmVtcGlyZS1qcy51cyUzQTEzMzcmdHI9dWRwJTNBJTJGJTJGdHJhY2tlci5sZWVjaGVycy1wYXJhZGlzZS5vcmclM0E2OTY5JnRyPXVkcCUzQSUyRiUyRnRyYWNrZXIub3BlbnRyYWNrci5vcmclM0ExMzM3JnRyPXdzcyUzQSUyRiUyRnRyYWNrZXIuYnRvcnJlbnQueHl6JnRyPXdzcyUzQSUyRiUyRnRyYWNrZXIuZmFzdGNhc3QubnomdHI9d3NzJTNBJTJGJTJGdHJhY2tlci5vcGVud2VidG9ycmVudC5jb20md3M9aHR0cHMlM0ElMkYlMkZ3ZWJ0b3JyZW50LmlvJTJGdG9ycmVudHMlMkYmeHM9aHR0cHMlM0ElMkYlMkZ3ZWJ0b3JyZW50LmlvJTJGdG9ycmVudHMlMkZzaW50ZWwudG9ycmVudA==&fmt=bXA0";
const YT_VALID = "plr=eXQ=&src=YWR1ZHF0cTJnQnc=&fmt=YW55";
const HTTP_VALID = "plr=aHR0cA==&src=aHR0cHM6Ly9leGFtcGxlLm9yZy8=&fmt=YW55";
const UNKNOWN_PLAYER = "plr=dW5rbm93bi1lbmdpbmU=&src=Zm9vOi8vYmFy&fmt=bXA0";



describe('MusicStoreMetaParser', ()=>{
    describe('constructor', ()=>{
        it('throws an error if unknown given player', ()=>{
            expect(()=>{
                new MusicStoreMetaParser(UNKNOWN_PLAYER);
            }).toThrowError(/is not a valid player backend/);
        });

        it('throws an error if player is webtorrent but no client was passed as argument', ()=>{
            expect(()=>{
                new MusicStoreMetaParser(WEBTORRENT_VALID);
            }).toThrowError("Player is WebTorrent but no client was passed to constructor.");
        });
    });

    describe('parse()', ()=>{
        it('returns expected player', ()=>{
            const clientSpy = jasmine.createSpy("webtorrentClient"); 
            const parentSpy = jasmine.createSpy("playerDOMParent");
            const optionsSpy = jasmine.createSpy("playerOptions");
            const playerSpy = jasmine.createSpy("player");
            spyOn(WebTorrentPlayer, "webtorrentPlayerFactory").and.callFake((client: WebTorrent.Instance, parent: string, options: any)=>{
                expect(client).toBe(clientSpy);
                expect(parent).toBe(parentSpy);
                expect(options).toBe(optionsSpy);
                return playerSpy;
            });

            const parser = new MusicStoreMetaParser(WEBTORRENT_VALID, <any>clientSpy, <any>parentSpy, <any>optionsSpy);
            expect(parser.parse().player).toBe(playerSpy);
        });
    });

    describe('parseAndSanitizeMetas()', ()=>{
        it("doesn't include webtorrent sources if not client", ()=>{
            const ret = MusicStoreMetaParser.parseAndSanitizeMetas(
                [WEBTORRENT_VALID, HTTP_VALID, WEBTORRENT_VALID, WEBTORRENT_VALID, YT_VALID]
            );
            expect(ret.length).toEqual(2);
            expect(ret.find(m=>m.meta.src === "webtorrent")).toBeUndefined();
        });

        it('is ordered by priority', ()=>{
            const clientSpy = jasmine.createSpy("client");
            const ret = MusicStoreMetaParser.parseAndSanitizeMetas(
                [WEBTORRENT_VALID, HTTP_VALID, YT_VALID, HTTP_VALID, HTTP_VALID, YT_VALID, WEBTORRENT_VALID],
                <any>clientSpy
            );
            expect(ret.length).toEqual(7);
            expect(ret.map(s=>s.meta.plr)).toEqual([
                "yt", "yt", "webtorrent", "webtorrent", "http", "http", "http"
            ]);
        });
    });
});