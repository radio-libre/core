import { NativeStoreFactory, NativeStoreMeta, NativeStoreQuerier } from '../meta';
import { Model } from '../../models';
import { IDBController } from '../../helpers/databases/idb';
import { IDBPDatabase } from 'idb';


export function idbQuerierFactoryFactory<T extends Model>(
    dbName = "music"
): NativeStoreFactory<NativeStoreQuerier<T>> {
    return async (meta: NativeStoreMeta) => {
        const db = await new IDBController({
            name: dbName,
            version: meta.ver,
            store: meta.nme
        }).open();
        return {
            async count() {
                return count(db, meta.nme);
            },
            async *cursor(query?: string) {
                return cursor<T>(db, meta.nme, query);
            },
            close() {
                return close(db);
            }
        };
    }
}

export async function count(db: IDBPDatabase, store: string): Promise<number> {
    return db.transaction(store, "readonly").store.count();
}

export async function* cursor<T>(db: IDBPDatabase, store: string, query?: string): AsyncGenerator<T, void, unknown> {
    let cursor = await db.transaction(store, "readonly").store.openCursor(query);
    while (cursor) {
        yield cursor.value;
        cursor = await cursor.continue();
    }
}

export function close(db: IDBPDatabase): void {
    return db.close();
}