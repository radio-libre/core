import { IDBController, IDBControllerOptions, getTestDbOptions } from "../../helpers/databases/idb";
import { count, cursor } from './idb';

describe('Querier - IDB', ()=>{
    let db: IDBController;
    let options: IDBControllerOptions;

    beforeEach(async (done) => {
        options = getTestDbOptions();
        db = new IDBController(options);
        await db.open();
        await db.clear();
        done();
    }, 20000);

    afterEach(async (done) => {
        db.close();
        await IDBController.deleteDBIfExists(options.name);
        done();
    }, 20000);

    it('returns expected count', async (done)=>{
        await db.populate(12);
        expect(await count(db.getConnection(), options.store)).toEqual(12);
        done();
    });

    it('iterates through all tracks', async (done)=>{
        await db.populate(12);
        let counter = 0;
        for await (const track of cursor(db.getConnection(), options.store)){
            counter++;
            expect((<any>track).id).toMatch(/[a-f0-9]{8}-[a-f0-9]{4}-4[a-f0-9]{3}-[89aAbB][a-f0-9]{3}-[a-f0-9]{12}/);
        }
        expect(counter).toEqual(12);
        done();
    });
});