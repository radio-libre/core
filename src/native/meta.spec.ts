import { NativeStoreMetaParser } from './meta';
import * as IDBImporter from './importer/idb';
import * as IDBQuerier from './querier/idb';

const VALID = "imp=aWRi&qry=aWRi&src=bm9uZQ==&nme=dGVzdHN0b3Jl&ver=MQ==&lnk=aHR0cHM6Ly9leGFtcGxlLmNvbS8=&eml=ZXhhbXBsZUBtYWlsLmNvbQ==";
const BAD_VERSION_NUMBER = "imp=aWRi&qry=aWRi&src=bm9uZQ==&nme=dGVzdHN0b3Jl&ver=aW52YWxpZC12ZXJzaW9uLW51bWJlcg==&lnk=aHR0cHM6Ly9leGFtcGxlLmNvbS8=&eml=ZXhhbXBsZUBtYWlsLmNvbQ==";
const UNKNOWN_QUERIER = "imp=aWRi&qry=dW5rbm93bi1lbmdpbmU=&src=bm9uZQ==&nme=dGVzdHN0b3Jl&ver=MQ==&lnk=aHR0cHM6Ly9leGFtcGxlLmNvbS8=&eml=ZXhhbXBsZUBtYWlsLmNvbQ==";
const UNKNOWN_IMPORTER = "imp=dW5rbm93bi1lbmdpbmU=&qry=aWRi&src=bm9uZQ==&nme=dGVzdHN0b3Jl&ver=MQ==&lnk=aHR0cHM6Ly9leGFtcGxlLmNvbS8=&eml=ZXhhbXBsZUBtYWlsLmNvbQ==";

describe('NativeStoreMetaParser', ()=>{
    describe('constructor', ()=>{
        it('throws an error if given version is not a number', ()=>{
            expect(()=>{
                new NativeStoreMetaParser(BAD_VERSION_NUMBER);
            }).toThrowError(/is not a valid version/);
        });

        it('throws an error if unknown given querier', ()=>{
            expect(()=>{
                new NativeStoreMetaParser(UNKNOWN_QUERIER);
            }).toThrowError(/is not a valid querier backend/);
        });

        it('throws an error if unknown given importer', ()=>{
            expect(()=>{
                new NativeStoreMetaParser(UNKNOWN_IMPORTER);
            }).toThrowError(/is not a valid importer backend/);
        });
    });

    describe('parse()', ()=>{
        it('returns expected querier factory', ()=>{
            const idbQuerierFactorySpy = jasmine.createSpy();
            spyOn(IDBQuerier, "idbQuerierFactoryFactory").and.callFake((dbname: string)=>{
                expect(dbname).toEqual("test-db-name");
                return idbQuerierFactorySpy;
            });

            const parser = new NativeStoreMetaParser(VALID, "test-db-name");
            const nativeStore = parser.parse();
            expect(nativeStore.querierFactory).toBe(idbQuerierFactorySpy);
        });

        it('returns expected importer factory', ()=>{
            const idbImporterFactorySpy = jasmine.createSpy();
            spyOn(IDBImporter, "idbImporterFactoryFactory").and.callFake((dbname: string, bucketSize: number)=>{
                expect(dbname).toEqual("test-db-name");
                expect(bucketSize).toEqual(648951);
                return idbImporterFactorySpy;
            });

            const parser = new NativeStoreMetaParser(VALID, "test-db-name", 648951);
            const nativeStore = parser.parse();
            expect(nativeStore.importerFactory).toBe(idbImporterFactorySpy);
        });

        it('returns the version as a number (not a string)', () => {
            const parser = new NativeStoreMetaParser(VALID);
            const nativeStore = parser.parse();
            expect(typeof nativeStore.meta.ver).toEqual("number");
            expect(nativeStore.meta.ver).toEqual(1);
        });
    });

    describe('createWriteStream()', ()=>{
        it('calls the importerFactory with the expected arguments', ()=>{
            const store = {
                importerFactory: jasmine.createSpy("importerFactory"),
                querierFactory: jasmine.createSpy("querierFactory"),
                meta: jasmine.createSpy("storeMeta")
            }
            NativeStoreMetaParser.createWriteStream(<any>store);

            expect(store.importerFactory).toHaveBeenCalledWith(store.meta);
            expect(store.querierFactory).not.toHaveBeenCalled();
        });
    });

    describe('createQuerier()', ()=>{
        it('calls the querierFactory with the expected arguments', ()=>{
            const store = {
                importerFactory: jasmine.createSpy("importerFactory"),
                querierFactory: jasmine.createSpy("querierFactory"),
                meta: jasmine.createSpy("storeMeta")
            }
            NativeStoreMetaParser.createQuerier(<any>store);

            expect(store.querierFactory).toHaveBeenCalledWith(store.meta);
            expect(store.importerFactory).not.toHaveBeenCalled();
        });
    });
});