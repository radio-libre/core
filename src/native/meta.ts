import { Writable } from "stream";
import { idbImporterFactoryFactory } from "./importer/idb";
import { idbQuerierFactoryFactory } from "./querier/idb";
import { MetaParser } from "../meta";

export type NativeStoreImporter = Writable;

export interface NativeStoreQuerier<T> {
    count(): Promise<number>;
    cursor(query?: string): AsyncIterable<Promise<T>>;
    close(): void;
} 

export type NativeStoreImportersMap = { [id: string]: NativeStoreFactory<NativeStoreImporter> }
export type NativeStoreQueriersMap<T> = { [id: string]: NativeStoreFactory<NativeStoreQuerier<T>> }

export type NativeStoreFactory<Target> = (meta: NativeStoreMeta) => Promise<Target>;


export interface NativeStoreMeta {
    imp: string; // Importer
    qry: string; // Query
    src: string; // Source (not used for IDB)
    nme: string; // Name
    ver: number; // Version
    lnk: string; // Info Link
    eml: string; // Report Email
}

export interface NativeStore<T> {
    importerFactory: NativeStoreFactory<NativeStoreImporter>;
    querierFactory: NativeStoreFactory<NativeStoreQuerier<T>>;
    meta: NativeStoreMeta;
}

export class NativeStoreMetaParser<T> extends MetaParser<NativeStoreMeta> {
    
    static createWriteStream<T>(store: NativeStore<T>): Promise<Writable> {
        return store.importerFactory(store.meta)
    }
    
    static createQuerier<T>(store: NativeStore<T>): Promise<NativeStoreQuerier<T>>{
        return store.querierFactory(store.meta);
    }

    /******************************
     * REGISTER THE IMPORTERS HERE
     ******************************/
    private NATIVE_STORE_IMPORTERS(): NativeStoreImportersMap {
        return {
            "idb": idbImporterFactoryFactory(this.dbName, this.bucketSize)
        };
    }

    /******************************
     * REGISTER THE QUERIERS HERE
     ******************************/
    private NATIVE_STORE_QUERIERS(): NativeStoreQueriersMap<T> {
        return {
            "idb": idbQuerierFactoryFactory(this.dbName)
        };
    }

    private importerFactory(): NativeStoreFactory<NativeStoreImporter> {
        const imp = this.NATIVE_STORE_IMPORTERS()[this.get("imp")];
        if (!imp) throw new Error(`${this.get("imp")} is not a valid importer backend.`);
        return imp;
    }

    private querierFactory(): NativeStoreFactory<NativeStoreQuerier<T>> {
        const qry = this.NATIVE_STORE_QUERIERS()[this.get("qry")];
        if (!qry) throw new Error(`${this.get("qry")} is not a valid querier backend.`);
        return qry;
    }

    private version(){
        const ver = parseInt(this.get("ver"));
        if (typeof ver !== 'number' || isNaN(ver)) throw new Error(`${this.get("ver")} is not a valid version.`);
        return ver;
    }

    constructor(public readonly query: string, private dbName = "music", private bucketSize = 10){
        super(query, [ "imp", "qry", "src", "nme", "ver", "lnk", "eml" ]);

        // Throw errors in constructor
        this.importerFactory() && this.querierFactory() && this.version();
    }
    
    parse(): NativeStore<T> {
        return {
            importerFactory: this.importerFactory(),
            querierFactory: this.querierFactory(),
            meta: {
                ...this.parseMeta(),
                ver: this.version()
            }
        }
    }
}