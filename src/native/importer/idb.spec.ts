import { IDBController, IDBControllerOptions, getTestDbOptions } from '../../helpers/databases/idb';
import { IDBStoreImporter } from './idb';
import { TrackStream } from '../../helpers/streams/tracks';

describe('Importers - IDB - IDBStoreImporter', () => {
    let db: IDBController;
    let options: IDBControllerOptions;

    beforeEach(async (done) => {
        options = getTestDbOptions();
        db = new IDBController(options);
        await db.open();
        await db.clear();
        done();
    }, 20000);

    afterEach(async (done) => {
        db.close();
        await IDBController.deleteDBIfExists(options.name);
        done();
    }, 20000);

    it('imports objects when bucket size is 1', async (done) => {
        const ws = new IDBStoreImporter(db.getConnection(), options.store, 1);

        new TrackStream(10)
            .pipe(ws)
            .on('finish', async () => {
                const count = await db.getConnection().count(options.store);
                expect(count).toEqual(10);
                
                ws.destroy();
                done();
            })
            .on('error', (err)=>{
                throw err;
            });
    }, 20000);

    it('imports objects when bucket size is 12', async (done) => {
        const ws = new IDBStoreImporter(db.getConnection(), options.store, 12);

        new TrackStream(25)
            .pipe(ws)
            .on('finish', async () => {
                const count = await db.getConnection().count(options.store);
                expect(count).toEqual(25);
                
                ws.destroy();
                done();
            })
            .on('error', (err)=>{
                throw err;
            });
    }, 20000);
});