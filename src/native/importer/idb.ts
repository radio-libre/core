import { IDBPDatabase } from 'idb';
import { Model } from '../../models';
import { Writable } from 'stream';
import { NativeStoreFactory, NativeStoreImporter, NativeStoreMeta } from '../meta';
import { IDBController } from '../../helpers/databases/idb';

export const idbImporterFactoryFactory: (
    dbName: string,
    bucketSize: number
) => NativeStoreFactory<NativeStoreImporter> = (
    dbName = "music",
    bucketSize = 10
) => {
    return async(meta: NativeStoreMeta)=>{
        const db = await new IDBController({
            name: dbName,
            version: meta.ver,
            store: meta.nme
        }).open();
        return new IDBStoreImporter(db, meta.nme, bucketSize);
    }
}

export class IDBStoreImporter<T extends Model> extends Writable {
    private bucket: T[];

    constructor(private db: IDBPDatabase, private storeName: string, private bucketSize: number){
        super({
            objectMode: true
        });
        this.bucket = [];
    }

    async flushBucket(): Promise<void> {
        const tx = this.db.transaction(this.storeName, "readwrite");
        for (const m of this.bucket){
            await tx.store.add(m);
        }
        this.bucket = [];
        return await tx.done;
    }

    async _write(chunk: T, enc: string, cb: (err?: Error)=>void): Promise<void> {
        this.bucket.push(chunk);
        if (this.bucket.length >= this.bucketSize){
            await this.flushBucket();
        }
        return cb();
    }

    async _final(cb: (err?: Error)=>void): Promise<void> {
        await this.flushBucket();
        cb();
    }

    _destroy(): void {
        this.db.close();
    }
}