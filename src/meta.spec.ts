import {MetaParser} from './meta';

interface FooMeta {
    foo: string;
    bar: string;
}

class FooMetaParser extends MetaParser<FooMeta> {
    constructor(public readonly query: string){
        super(query, ["foo", "bar"]);
    }

    public parse(): { meta: FooMeta; } {
        return {
            meta: this.parseMeta()
        }
    }
}

describe('MetaParser', ()=>{
    it('encodes properly the metadata', ()=>{
        const encoded = FooMetaParser.encode<FooMeta>({
            foo: 'foo-value',
            bar: 'bar-value'
        });
        expect(encoded).toEqual("foo=Zm9vLXZhbHVl&bar=YmFyLXZhbHVl");
    });

    it('throws an error if one of the given keys is missing', ()=>{
        expect(()=>{
            new FooMetaParser("foo=Zm9vLXZhbHVl");
        }).toThrowError(/is not a valid FooMetaParser string/)
    });

    it('throws an error if a given value is not a valid base64 string', ()=>{
        expect(()=>{
            new FooMetaParser("foo=foo-value&bar=bar-value");
        }).toThrowError("String contains an invalid character");
    });

    it('returns the expected meta object', ()=>{
        const parser = new FooMetaParser("foo=Zm9vLXZhbHVl&bar=YmFyLXZhbHVl");
        const {meta} = parser.parse();
        expect(meta.foo).toEqual("foo-value");
        expect(meta.bar).toEqual("bar-value");
    });
});