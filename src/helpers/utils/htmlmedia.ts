export async function htmlMediaIsPlayable(media: HTMLMediaElement): Promise<boolean> {
    try {
        /**
         *  We mute to be able to play the media
         *  without interacting with browser.
         *  @see https://developer.mozilla.org/en-US/docs/Web/Media/Autoplay_guide
         */
        media.muted = true;
        await media.play();
        return true;
    } catch (err) {
        console.log(err.message);
        return false;
    }
}