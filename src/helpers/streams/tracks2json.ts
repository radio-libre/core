import { Transform } from "stream";
import { Track } from "../../models";

export class Track2JsonStream extends Transform {
    private counter: number;
    
    constructor(private size: number){
        super({
            objectMode: true
        });
        this.counter = 0;
    }

    _transform(chunk: Track, enc: string, cb: (reason?:any)=>any): void {
        let ret = "";
        if (this.counter === 0){
            ret += "{";
        }

        ret += `"${this.counter}": ${JSON.stringify(chunk)}`;
        
        if (this.counter + 1 === this.size){
            ret += "}";
        } else {
            ret += ",";
        }

        this.counter++;
        this.push(ret);
        cb();
    }
}