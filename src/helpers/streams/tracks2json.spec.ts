import { Track } from "../../models";
import { generateTrack } from "./tracks";
import { Track2JsonStream } from './tracks2json';

describe('Helpers - Streams - Tracks2JsonStream', ()=>{
    let tracks: Track[];
    const SIZE = 20;
    beforeEach(()=>{
        tracks = [];
        for (let i=0; i<SIZE; i++){
            tracks.push(generateTrack());
        }
    });
    
    it('encodes valid json', (done)=>{
        let json = "";
        const tr = new Track2JsonStream(SIZE);
        tr.on('data', (chunk)=>{
            json += chunk;
        });
        tr.on('end', ()=>{
            expect(()=>{
                JSON.parse(json);
            }).not.toThrowError();
            done();
        });
        
        for (const track of tracks){
            tr.write(track);
        }
        tr.end();
    });
});