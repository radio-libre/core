import { Readable } from "stream";
import { Track } from '../../models';
import { v4 as uuid } from 'uuid';
import { LoremIpsum } from 'lorem-ipsum';

const lorem = new LoremIpsum({
    sentencesPerParagraph: {
        max: 8,
        min: 4
    },
    wordsPerSentence: {
        max: 16,
        min: 4
    }
});

/**
 * Minimum is included and maximum is excluded
 */
export function generateNumber(min: number, max: number): number {
    return min + Math.floor(Math.random() * (max-min));
}

export function generateTrack(): Track {
    return {
        id: uuid(),
        title: lorem.generateWords(generateNumber(1, 5)),
        artist: lorem.generateWords(generateNumber(1,4)),
        album: lorem.generateWords(generateNumber(1,5)),
        genre: generateNumber(0, 192),
        year: generateNumber(1940, 2021),
        comment: lorem.generateSentences(generateNumber(1, 3)),
        trackNumber: generateNumber(0, 20),
        srcs: []
    }
}

export class TrackStream extends Readable {
    private counter: number;
    constructor(private size: number){
        super({
            objectMode: true
        });
        this.counter = 0;
    }

    _read(): void {
        if (this.counter >= this.size) {
            this.push(null);
            return;
        }
        this.push(generateTrack());
        this.counter++;
    }
}