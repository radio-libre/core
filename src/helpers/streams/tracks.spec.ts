import { TrackStream } from './tracks';

describe('Helpers - Streams - TrackStream', () => {
    it('generates the expected number of tracks', (done) => {
        const n = 12;
        let count = 0;
        const rs = new TrackStream(n);
        rs.on('end', () => {
            expect(count).toEqual(n);
            done();
        });
        rs.on('data', () => {
            count++;
        });
    });

    it('generates well-formed tracks', (done) => {
        const rs = new TrackStream(12);
        rs.on('end', () => {
            done();
        });
        rs.on('data', (track) => {
            // uuid v4 regex
            expect(track.id).toMatch(/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i);
            expect(track.title).toBeTruthy();
            expect(track.artist).toBeTruthy();
            expect(track.album).toBeTruthy();
            expect(track.genre).toBeLessThan(192);
            expect(track.year).toBeLessThan(2021);
            expect(track.comment).toBeTruthy();
            expect(track.trackNumber).toBeLessThan(20);
            expect(track.srcs).toBeTruthy();
        });
    });
});