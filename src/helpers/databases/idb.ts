import { IDBPDatabase, openDB, deleteDB } from "idb";
import { generateTrack } from "../streams/tracks";

export interface IDBControllerOptions {
    name: string;
    version: number;
    store: string;
}

let testDbCounter = 0;

function getTestDbName(): string {
    return `testdb_${testDbCounter++}`;
}

export function getTestDbOptions(): IDBControllerOptions {
    return {
        name: getTestDbName(),
        version: 1,
        store: 'test-store'
    }
}

export class IDBController {
    static async deleteDBIfExists(name: string): Promise<void> {
        try {
            return deleteDB(name, {
                blocked: ()=>{
                    throw new Error("There is an openned connection to database " + name + ". Cannot delete.");
                }
            });
        } catch (err) {
            return
        }
    }

    private db: IDBPDatabase | null;

    constructor(private options: IDBControllerOptions){
        this.db = null;
    }
    
    getConnection(): IDBPDatabase | null {
        return this.db;
    }

    async open(): Promise<IDBPDatabase> {
        this.db = await openDB(this.options.name, this.options.version, {
            upgrade: (db, oldVersion, newVersion) => {
                if (newVersion > 1)
                    throw new Error(`Requested version ${newVersion} for IDB database is invalid!`);
                db.createObjectStore(this.options.store, { keyPath: "id" });
            }
        });
        return this.db;
    }

    close(): void {
        this.db.close();
    }

    async clear(): Promise<void> {
        const tx = this.db.transaction(this.options.store, "readwrite");
        await tx.store.clear();
        return await tx.done;
    }

    async populate(size: number): Promise<void> {
        const tx = this.db.transaction(this.options.store, "readwrite");
        for (let i = 0; i<size; i++){
            tx.store.add(generateTrack());
        }
        return await tx.done;
    }
}