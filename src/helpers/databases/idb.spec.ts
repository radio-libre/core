import { IDBController, IDBControllerOptions, getTestDbOptions } from './idb';


describe('Helpers - Databases - IDBController', () => {
    let db: IDBController;
    let options: IDBControllerOptions;

    beforeEach(async (done)=>{
        options = getTestDbOptions();
        db = new IDBController(options);
        await db.open();
        done();
    }, 20000);

    afterEach(async (done)=>{
        db.close();
        await IDBController.deleteDBIfExists(options.name);
        done();
    }, 20000);

    it('adds n tracks to database', async (done)=>{
        await db.populate(12);
        const count = await db.getConnection().count(options.store);
        expect(count).toEqual(12);
        done();
    });

    it('clears the database', async (done)=>{
        await db.populate(12);
        await db.clear();
        const count = await db.getConnection().count(options.store);
        expect(count).toEqual(0);
        done();
    });
});