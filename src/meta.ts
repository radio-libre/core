/**
 * Base class to decode a Store definition string. These strings are of the form:
 * `key=<base64_encoded_value>&otherkey=<base64_encoded_value>&...`
 */
export abstract class MetaParser<IMeta> extends URLSearchParams {
    public abstract parse(): {meta: IMeta};
    
    static encode<IMeta>(meta: IMeta): string {
        let s = "";
        const keys = Object.keys(meta);
        for (let i=0; i<keys.length; i++){
            const key = keys[i];
            const value = meta[key].toString();
            s += `${i !== 0 ? "&" : ""}${key}=${btoa(value)}`
        }
        return s
    }

    protected parseMeta(): IMeta {
        const meta: any = {};
        for (const k of this.keys){
            meta[k] = this.get(k.toString());
        }
        return meta;
    }
    
    get(key: string): string {
        return atob(super.get(key));
    }

    constructor(public readonly query: string, private keys: (keyof IMeta)[]){
        super(query);
        for (const k of this.keys){
            if (!this.has(k.toString())){ 
                throw new Error(`${this.query} is not a valid ${this.constructor.name} string`);
            } else {
                // throws an error if the value is not a base64 string
                this.get(k.toString());
            }
        }
    }
}