import { Readable, Writable, Duplex, Transform } from 'stream';

export interface Model {
    id?: string;
}

export interface Track extends Model {
    title: string;
    artist: string;
    album: string;
    year: number;
    comment: string;
    genre: number;
    trackNumber: number;
    srcs: string[];
}


/** From `stream-chain` package. Redeclared because not exported. */
export type TransformFunction = (chunk: any, encoding?: string) => any;

/** From `stream-chain` package. Redeclared because not exported. */
export type Stream = Readable | Writable | Duplex | Transform;

/** From `stream-chain` package. Redeclared because not exported. */
export type StreamItem = Stream | TransformFunction;
