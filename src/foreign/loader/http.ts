import * as miniget from 'miniget';
import { ForeignStoreFactory, ForeignStoreLoader, ForeignStoreMeta } from '../meta';

export const httpLoaderFactory: ForeignStoreFactory<ForeignStoreLoader> =
async (meta: ForeignStoreMeta): Promise<ForeignStoreLoader> => {
    return miniget(meta.src);
}
