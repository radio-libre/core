import { httpLoaderFactory } from './http';
import * as DevServerInfo from '../../../test-server-info.spec.json';
import { ForeignStoreMeta } from '../meta';

const META: ForeignStoreMeta = {
    ldr: '',
    dec: '',
    psr: '',
    src: '',
    nme: '',
    ver: 1,
    lnk: '',
    eml: '',
}

describe('Loader - HTTP', () => {
    let meta: ForeignStoreMeta;
    beforeEach(() => {
        meta = Object.create(META);
    });

    it('loads if source is reachable', async (done) => {
        meta.src = `http://localhost:${DevServerInfo.json.http.port}/`;
        const rs = await httpLoaderFactory(meta);
        let data = "";
        rs
        .on('error', (err)=>{
            throw err;
        })
        .on('data', (chunk:string) => {
            data += chunk;
        })
        .on('end', ()=>{
            expect(()=>{
                JSON.parse(data)
            }).not.toThrowError();
            done();
        });
    }, 15000);

    it('fails if source is unreachable', async (done)=>{
        meta.src = 'http://127.0.0.2/';
        const rs = await httpLoaderFactory(meta);
        let data = "";
        rs
        .on('error', (err)=>{
            expect(err.message).toEqual("NetworkError when attempting to fetch resource.");
            done();
        })
        .on('data', (chunk:string) => {
            data += chunk;
        })
        .on('end', ()=>{
            console.error("Received unexpected data: ", data);
            expect(1).toEqual(2);
        });
    }, 60000);
});