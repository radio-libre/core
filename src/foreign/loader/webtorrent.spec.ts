import { ForeignStoreMeta, ForeignStoreLoader, ForeignStoreFactory } from "../meta";
import { webtorrentLoaderFactoryFactory } from './webtorrent';
import * as DevServerInfo from '../../../test-server-info.spec.json';
import * as WebTorrent from "webtorrent";

const META: ForeignStoreMeta = {
    ldr: '',
    dec: '',
    psr: '',
    src: '',
    nme: '',
    ver: 1,
    lnk: '',
    eml: '',
}

describe('Loader - WebTorrent', () => {
    let meta: ForeignStoreMeta;
    let client: WebTorrent.Instance;
    let webtorrentLoaderFactory: ForeignStoreFactory<ForeignStoreLoader>;
    beforeEach(() => {
        meta = Object.create(META);
        client = new WebTorrent();
        webtorrentLoaderFactory = webtorrentLoaderFactoryFactory(client);
    });

    it('loads if source is reachable', async (done) => {
        meta.src = DevServerInfo.json.webtorrent;
        const rs = await webtorrentLoaderFactory(meta);
        let data = "";
        rs
            .on('error', (err) => {
                if (err.message !== 'RTCPeerConnection is gone (did you enter Offline mode?)') {
                    throw err;
                }
            })
            .on('data', (chunk: string) => {
                data += chunk;
            })
            .on('end', () => {
                expect(() => {
                    JSON.parse(data)
                }).not.toThrowError();
                done();
            });
    }, 120000);

    it('fails if source is unreachable', async (done) => {
        meta.src = "not-a-magnet";
        const rs = await webtorrentLoaderFactory(meta);
        let data = "";
        rs
            .on('error', (err) => {
                if (err.message !== 'RTCPeerConnection is gone (did you enter Offline mode?)') {
                    expect(err).toBeTruthy();
                    done();
                }
            })
            .on('data', (chunk: string) => {
                data += chunk;
            })
            .on('end', () => {
                throw new Error("Unexpected data was received:  " + data);
            });
    }, 120000);

    afterEach((done) => {
        client.destroy((err) => {
            if (err) throw err;
            done();
        });
    });

});