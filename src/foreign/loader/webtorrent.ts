import * as WebTorrent from 'webtorrent';
import { Readable } from 'stream';
import { ForeignStoreFactory, ForeignStoreLoader, ForeignStoreMeta } from '../meta';

export function webtorrentLoaderFactoryFactory(
    client: WebTorrent.Instance,
    findFunc: (file: WebTorrent.TorrentFile) => boolean = ()=>true
): ForeignStoreFactory<ForeignStoreLoader> {
    return (meta: ForeignStoreMeta)=>{
        return new Promise<Readable>((resolve)=>{
            const errorHandler = (err: Error)=>{
                resolve(new Readable({
                    read(){
                        this.emit('error', err);
                    }
                }));
            };
            client.on('error', errorHandler);
            client.add(meta.src, function (torrent) {
                const f = torrent.files.find( findFunc );
                const rs = new Readable();
                rs.wrap(f.createReadStream());
                client.removeListener('error', errorHandler);
                resolve(rs);
            });
        });
    }
}