import { parser } from 'stream-json/Parser';
import { pick } from 'stream-json/filters/Pick';
import { streamValues } from 'stream-json/streamers/StreamValues';
import { ForeignStoreFactory, ForeignStoreParser } from '../meta';

export const jsonParserFactory: ForeignStoreFactory<ForeignStoreParser> =
async (): Promise<ForeignStoreParser> => {
    return [
        parser(),
        pick({filter: /\d+/i}),
        streamValues(),
        (data: any)=>data.value
    ]
}