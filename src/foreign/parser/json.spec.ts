import {jsonParserFactory} from './json';
import { ForeignStoreMeta } from '../meta';
import { Readable } from 'stream';
import { chain } from 'stream-chain';

const META: ForeignStoreMeta = {
    ldr: '',
    dec: '',
    psr: '',
    src: '',
    nme: '',
    ver: 1,
    lnk: '',
    eml: '',
}

const DATA = {
    a: 1,
    b: "string",
    nested: {
        c: true,
        d: null
    }
};

describe('Parser - JSON', ()=>{
    it('parses if valid input', async (done)=>{
        const rs = new Readable({read(){}});
        const parser = chain([
            ...(await jsonParserFactory(META)),
            (decoded)=>{
                expect(decoded).toEqual(DATA);
                done();
            }
        ]);

        rs.pipe(parser);
        rs.push(`{"0": ${JSON.stringify(DATA)}}`);
    });

    it('throws an error if invalid input', async (done)=>{
        const rs = new Readable({read(){}});
        const parser = chain([
            ...(await jsonParserFactory(META)),
            (decoded)=>{
                throw new Error("Parsed unexpected data: "+ JSON.stringify(decoded));
            }
        ]).on('error', (err)=>{
            expect(err.message).toMatch(/Parser cannot parse input/);
            done();
        });

        rs.pipe(parser);
        rs.push(`{"0":, ${JSON.stringify(DATA)}}`);
    });
});