import { Readable } from "stream";
import { StreamItem } from "../models";
import { httpLoaderFactory } from "./loader/http";
import { webtorrentLoaderFactoryFactory } from "./loader/webtorrent";
import WebTorrent = require("webtorrent");
import { gzipDecoderFactory } from "./decoder/gzip";
import { noneDecoderFactory } from "./decoder/none";
import { jsonParserFactory } from "./parser/json";
import { MetaParser } from "../meta";
import Chain = require("stream-chain");

export type ForeignStoreFactory<Target> = (meta: ForeignStoreMeta) => Promise<Target>;
export type ForeignStoreLoader = Readable;
export type ForeignStoreDecoder = StreamItem[];
export type ForeignStoreParser = StreamItem[];

export type ForeignStoreLoadersMap = {[id: string]: ForeignStoreFactory<ForeignStoreLoader>};
export type ForeignStoreDecodersMap = {[id: string]: ForeignStoreFactory<ForeignStoreDecoder>};
export type ForeignStoreParsersMap = {[id: string]: ForeignStoreFactory<ForeignStoreParser>};

export interface ForeignStoreMeta {
    ldr: string; // Loader
    dec: string; // Decoder
    psr: string; // Parser
    src: string; // Source
    nme: string; // Name
    ver: number; // Version
    lnk: string; // Info Link
    eml: string; // Report Email
}

export interface ForeignStore {
    loaderFactory: ForeignStoreFactory<ForeignStoreLoader>;
    decoderFactory: ForeignStoreFactory<ForeignStoreDecoder>;
    parserFactory: ForeignStoreFactory<ForeignStoreParser>;
    meta: ForeignStoreMeta;
}

export class ForeignStoreMetaParser extends MetaParser<ForeignStoreMeta> {
    
    static async createReadStream(s: ForeignStore): Promise<Chain> {
        return Chain.chain([
            ...([await s.loaderFactory(s.meta)] || []),
            ...(await s.decoderFactory(s.meta) || []),
            ...(await s.parserFactory(s.meta) || []),
        ]);
    }
    
    /******************************
     * REGISTER THE LOADERS HERE
     ******************************/
    private FOREIGN_STORE_LOADERS(): ForeignStoreLoadersMap {
        return {
            "http": httpLoaderFactory,
            "webtorrent": webtorrentLoaderFactoryFactory(this.client)
        };
    }
    
    /******************************
     * REGISTER THE DECODERS HERE
     ******************************/
    private FOREIGN_STORE_DECODERS(): ForeignStoreDecodersMap {
        return {
            "gzip": gzipDecoderFactory,
            "none": noneDecoderFactory
        };
    }

    /******************************
     * REGISTER THE PARSERS HERE
     ******************************/
    private FOREIGN_STORE_PARSERS(): ForeignStoreParsersMap {
        return {
            "json": jsonParserFactory
        };
    }

    
    private loaderFactory(): ForeignStoreFactory<ForeignStoreLoader> {
        if (this.get("ldr") === "webtorrent" && !this.client)
        throw new Error(`Requested loader is WebTorrent but not client was passed in constructor.`);
        const ldr = this.FOREIGN_STORE_LOADERS()[this.get("ldr")];
        if (!ldr) throw new Error(`${this.get("ldr")} is not a valid loader backend.`);
        return ldr;
    }
    
    private decoderFactory(): ForeignStoreFactory<ForeignStoreDecoder> {
        const dec = this.FOREIGN_STORE_DECODERS()[this.get("dec")];
        if (!dec) throw new Error(`${this.get("dec")} is not a valid decoder backend.`);
        return dec;
    }
    
    private parserFactory(): ForeignStoreFactory<ForeignStoreParser> {
        const psr = this.FOREIGN_STORE_PARSERS()[this.get("psr")];
        if (!psr) throw new Error(`${this.get("psr")} is not a valid parser backend.`);
        return psr;
    }
    
    private version(){
        const ver = parseInt(this.get("ver"));
        if (isNaN(ver)) throw new Error(`${this.get("ver")} is not a valid version.`);
        return ver;
    }
    
    constructor(public readonly query: string, private client: WebTorrent.Instance = null){
        super(query, [ "ldr", "dec", "psr", "src", "nme", "ver", "lnk", "eml" ]);

        // Throw errors in constructor
        this.loaderFactory() && this.decoderFactory() && this.parserFactory() && this.version();
    }

    parse(): ForeignStore {
        return {
            loaderFactory: this.loaderFactory(),
            decoderFactory: this.decoderFactory(),
            parserFactory: this.parserFactory(),
            meta: {
                ...this.parseMeta(),
                ver: this.version()
            }
        }
    }
}