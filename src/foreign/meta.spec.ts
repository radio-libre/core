import {ForeignStoreMetaParser} from './meta';
import * as WebTorrentLoader from './loader/webtorrent';
import * as NoneDecoder from './decoder/none';
import * as JSONParser from './parser/json';
import WebTorrent = require('webtorrent');
import StreamChain = require('stream-chain');

const VALID = "ldr=d2VidG9ycmVudA==&dec=bm9uZQ==&psr=anNvbg==&src=bWFnbmV0Oj94dD11cm46YnRpaDowOGFkYTVhN2E2MTgzYWFlMWUwOWQ4MzFkZjY3NDhkNTY2MDk1YTEwJmRuPVNpbnRlbCZ0cj11ZHAlM0ElMkYlMkZleHBsb2RpZS5vcmclM0E2OTY5JnRyPXVkcCUzQSUyRiUyRnRyYWNrZXIuY29wcGVyc3VyZmVyLnRrJTNBNjk2OSZ0cj11ZHAlM0ElMkYlMkZ0cmFja2VyLmVtcGlyZS1qcy51cyUzQTEzMzcmdHI9dWRwJTNBJTJGJTJGdHJhY2tlci5sZWVjaGVycy1wYXJhZGlzZS5vcmclM0E2OTY5JnRyPXVkcCUzQSUyRiUyRnRyYWNrZXIub3BlbnRyYWNrci5vcmclM0ExMzM3JnRyPXdzcyUzQSUyRiUyRnRyYWNrZXIuYnRvcnJlbnQueHl6JnRyPXdzcyUzQSUyRiUyRnRyYWNrZXIuZmFzdGNhc3QubnomdHI9d3NzJTNBJTJGJTJGdHJhY2tlci5vcGVud2VidG9ycmVudC5jb20md3M9aHR0cHMlM0ElMkYlMkZ3ZWJ0b3JyZW50LmlvJTJGdG9ycmVudHMlMkYmeHM9aHR0cHMlM0ElMkYlMkZ3ZWJ0b3JyZW50LmlvJTJGdG9ycmVudHMlMkZzaW50ZWwudG9ycmVudA==&nme=dGVzdC1zb3VyY2UtZGI=&ver=MQ==&lnk=aHR0cHM6Ly9leGFtcGxlLmNvbS8=&eml=ZXhhbXBsZUBtYWlsLmNvbQ==";
const BAD_VERSION_NUMBER = "ldr=d2VidG9ycmVudA==&dec=bm9uZQ==&psr=anNvbg==&src=bWFnbmV0Oj94dD11cm46YnRpaDowOGFkYTVhN2E2MTgzYWFlMWUwOWQ4MzFkZjY3NDhkNTY2MDk1YTEwJmRuPVNpbnRlbCZ0cj11ZHAlM0ElMkYlMkZleHBsb2RpZS5vcmclM0E2OTY5JnRyPXVkcCUzQSUyRiUyRnRyYWNrZXIuY29wcGVyc3VyZmVyLnRrJTNBNjk2OSZ0cj11ZHAlM0ElMkYlMkZ0cmFja2VyLmVtcGlyZS1qcy51cyUzQTEzMzcmdHI9dWRwJTNBJTJGJTJGdHJhY2tlci5sZWVjaGVycy1wYXJhZGlzZS5vcmclM0E2OTY5JnRyPXVkcCUzQSUyRiUyRnRyYWNrZXIub3BlbnRyYWNrci5vcmclM0ExMzM3JnRyPXdzcyUzQSUyRiUyRnRyYWNrZXIuYnRvcnJlbnQueHl6JnRyPXdzcyUzQSUyRiUyRnRyYWNrZXIuZmFzdGNhc3QubnomdHI9d3NzJTNBJTJGJTJGdHJhY2tlci5vcGVud2VidG9ycmVudC5jb20md3M9aHR0cHMlM0ElMkYlMkZ3ZWJ0b3JyZW50LmlvJTJGdG9ycmVudHMlMkYmeHM9aHR0cHMlM0ElMkYlMkZ3ZWJ0b3JyZW50LmlvJTJGdG9ycmVudHMlMkZzaW50ZWwudG9ycmVudA==&nme=dGVzdC1zb3VyY2UtZGI=&ver=aW52YWxpZC12ZXJzaW9u&lnk=aHR0cHM6Ly9leGFtcGxlLmNvbS8=&eml=ZXhhbXBsZUBtYWlsLmNvbQ==";
const UNKNOWN_LOADER = "ldr=dW5rbm93bi1lbmdpbmU=&dec=bm9uZQ==&psr=anNvbg==&src=Zm9vOi8vYmFy&nme=dGVzdC1zb3VyY2UtZGI=&ver=aW52YWxpZC12ZXJzaW9u&lnk=aHR0cHM6Ly9leGFtcGxlLmNvbS8=&eml=ZXhhbXBsZUBtYWlsLmNvbQ==";
const UNKNOWN_DECODER = "ldr=d2VidG9ycmVudA==&dec=dW5rbm93bi1lbmdpbmU=&psr=anNvbg==&src=Zm9vOi8vYmFy&nme=dGVzdC1zb3VyY2UtZGI=&ver=aW52YWxpZC12ZXJzaW9u&lnk=aHR0cHM6Ly9leGFtcGxlLmNvbS8=&eml=ZXhhbXBsZUBtYWlsLmNvbQ==";
const UNKNOWN_PARSER = "ldr=d2VidG9ycmVudA==&dec=bm9uZQ==&psr=dW5rbm93bi1lbmdpbmU=&src=Zm9vOi8vYmFy&nme=dGVzdC1zb3VyY2UtZGI=&ver=aW52YWxpZC12ZXJzaW9u&lnk=aHR0cHM6Ly9leGFtcGxlLmNvbS8=&eml=ZXhhbXBsZUBtYWlsLmNvbQ==";

describe('ForeignStoreMetaParser', ()=>{
    let clientSpy;
    beforeEach(()=>{
        clientSpy = jasmine.createSpy("webTorrentClient");
    });

    describe('constructor', ()=>{
        it('throws an error if given version is not a number', ()=>{
            expect(()=>{
                new ForeignStoreMetaParser(BAD_VERSION_NUMBER, clientSpy)
            }).toThrowError(/is not a valid version/);
        });

        it('throws an error if using webtorrent but no client is passed as argument', ()=>{
            expect(()=>{
                new ForeignStoreMetaParser(VALID)
            }).toThrowError("Requested loader is WebTorrent but not client was passed in constructor.");
        });

        it('throws an error if unknown given loader', ()=>{
            expect(()=>{
                new ForeignStoreMetaParser(UNKNOWN_LOADER, clientSpy);
            }).toThrowError(/is not a valid loader backend/);
        });

        it('throws an error if unknown given decoder', ()=>{
            expect(()=>{
                new ForeignStoreMetaParser(UNKNOWN_DECODER, clientSpy);
            }).toThrowError(/is not a valid decoder backend/);
        });

        it('throws an error if unknown given parser', ()=>{
            expect(()=>{
                new ForeignStoreMetaParser(UNKNOWN_PARSER, clientSpy);
            }).toThrowError(/is not a valid parser backend/);
        });
    });

    describe('parse()', ()=>{
        it('returns expected loader factory', ()=>{
            const webtorrentLoaderFactorySpy = jasmine.createSpy();
            spyOn(WebTorrentLoader, "webtorrentLoaderFactoryFactory").and.callFake((client: WebTorrent.Instance)=>{
                expect(client).toBe(clientSpy);
                return webtorrentLoaderFactorySpy;
            });
            const parser = new ForeignStoreMetaParser(VALID, clientSpy);
            const foreignStore = parser.parse();
            expect(foreignStore.loaderFactory).toBe(webtorrentLoaderFactorySpy);
        });

        it('returns expected decoder factory', ()=>{
            const noneDecoderFactorySpy = spyOn(NoneDecoder, "noneDecoderFactory");
            const parser = new ForeignStoreMetaParser(VALID, clientSpy);
            const foreignStore = parser.parse();
            expect(foreignStore.decoderFactory).toBe(noneDecoderFactorySpy);
        });

        it('returns expected parser factory', ()=>{
            const jsonParserFactorySpy = spyOn(JSONParser, "jsonParserFactory");
            const parser = new ForeignStoreMetaParser(VALID, clientSpy);
            const foreignStore = parser.parse();
            expect(foreignStore.parserFactory).toBe(jsonParserFactorySpy);
        });

        it('returns the version as a number (not a string)', ()=>{
            const parser = new ForeignStoreMetaParser(VALID, clientSpy);
            const foreignStore = parser.parse();
            expect(typeof foreignStore.meta.ver).toEqual('number');
            expect(foreignStore.meta.ver).toEqual(1);
        });
    });

    describe('createReadStream()', ()=>{
        it('calls the factories with the expected arguments', async ()=>{
            const store = {
                loaderFactory: jasmine.createSpy("loaderFactory"),
                decoderFactory: jasmine.createSpy("decoderFactory"),
                parserFactory: jasmine.createSpy("parserFactory"),
                meta: jasmine.createSpy("meta")
            }
            const returnChainSpy = jasmine.createSpy("returnChain");
            spyOn(StreamChain, "chain").and.callFake(()=>{
                return returnChainSpy;
            });

            const ret = await ForeignStoreMetaParser.createReadStream(<any>store);
            expect(ret).toBe(returnChainSpy);
            expect(store.loaderFactory).toHaveBeenCalledWith(store.meta);
            expect(store.decoderFactory).toHaveBeenCalledWith(store.meta);
            expect(store.parserFactory).toHaveBeenCalledWith(store.meta);
        });
    });
});