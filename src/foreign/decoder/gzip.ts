import { createGunzip } from "zlib";
import { ForeignStoreFactory, ForeignStoreDecoder } from '../meta';

export const gzipDecoderFactory: ForeignStoreFactory<ForeignStoreDecoder> =
async (): Promise<ForeignStoreDecoder> => {
    return [
        createGunzip()
    ]
}