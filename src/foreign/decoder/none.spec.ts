import {noneDecoderFactory} from './none';
import { Readable } from 'stream';
import { ForeignStoreMeta } from '../meta';
import { chain } from 'stream-chain';


const META: ForeignStoreMeta = {
    ldr: '',
    dec: '',
    psr: '',
    src: '',
    nme: '',
    ver: 1,
    lnk: '',
    eml: '',
}

const DATA = "This is a test string";

describe('Decoder - None', ()=>{
    it('does nothing', async (done)=>{
        const rs = new Readable({read(){}});
        const decoder = chain([
            ...(await noneDecoderFactory(META)),
            (decoded)=>{
                expect(decoded.toString()).toEqual(DATA);
                done();
            }
        ]);

        rs.pipe(decoder);
        rs.push(DATA)
    });
});