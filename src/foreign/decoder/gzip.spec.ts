import {gzipDecoderFactory} from './gzip';
import { Readable } from 'stream';
import { ForeignStoreMeta } from '../meta';
import { chain } from 'stream-chain';

const DATA = {
    plain: "This is a test string",
    base64: "H4sIAAAAAAAAAwvJyCxWAKJEhZLU4hKF4pKizLx0AG3zTmsVAAAA"
}

const META: ForeignStoreMeta = {
    ldr: '',
    dec: '',
    psr: '',
    src: '',
    nme: '',
    ver: 1,
    lnk: '',
    eml: '',
}

describe('Decoder - Gzip', ()=>{
    it('decodes if valid input', async (done)=>{
        const rs = new Readable({read(){}});
        const decoder = chain([
            ...(await gzipDecoderFactory(META)),
            (decoded)=>{
                expect(decoded.toString()).toEqual(DATA.plain);
                done();
            }
        ]);

        rs.pipe(decoder);
        rs.push(new Buffer(DATA.base64, 'base64'))
    });

    it('throws an error if invalid input', async (done)=>{
        const rs = new Readable({read(){}});
        const decoder = chain([
            ...(await gzipDecoderFactory(META)),
            (decoded)=>{
                throw new Error("Unexpected data was decoded: " + decoded.toString());
            }
        ]).on('error', (err)=>{
            expect(err.message).toEqual("incorrect header check");
            done();
        });

        rs.pipe(decoder);
        rs.push(DATA.plain);
    });
});