import { ForeignStoreFactory, ForeignStoreDecoder } from '../meta';

export const noneDecoderFactory: ForeignStoreFactory<ForeignStoreDecoder> =
async (): Promise<ForeignStoreDecoder> => {
    return []
}